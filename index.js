import { Programmer } from './classes/Programmer.js'

const programmer1 = new Programmer('Volodymyr', 47, 2500, 'Ukrainian');
const programmer2 = new Programmer('Oleksandr', 34, 3600, 'English');
const programmer3 = new Programmer('Anatoly', 25, 1800, 'Polish');

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);