'use strict'

export class Employee {
	constructor(name, age, salary) {
		this.name = name;
		this.age = age;
		this.salary = salary;
	}
	get name(){
        return this._name;
    }
    set name(value){
        this._name = value;
    }
    get age(){
        return this._age;
    }
    set age(value){
        this._age = value;
    }
    get salary(){
        return this._asalary;
    }
    set salary(value){
        this._salary = value;
    }

}